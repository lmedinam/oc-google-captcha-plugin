<?php namespace Medinam\GoogleCaptcha\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWhitelistIpsTable extends Migration
{
    public function up()
    {
        Schema::create('medinam_googlecaptcha_whitelist_ips', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('note');
            $table->ipAddress('ip');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('medinam_googlecaptcha_whitelist_ips');
    }
}
