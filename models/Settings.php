<?php namespace Medinam\GoogleCaptcha\Models;

use Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'medinam_googlecaptcha_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
