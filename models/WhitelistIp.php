<?php namespace Medinam\GoogleCaptcha\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * WhitelistIp Model
 */
class WhitelistIp extends Model
{
    use Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'medinam_googlecaptcha_whitelist_ips';

    /**
     * Validation rules
     */
    public $rules = [
        'note' => 'nullable',
        'ip' => 'required|ip'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['note', 'ip'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
