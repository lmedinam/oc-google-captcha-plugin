<?php namespace Medinam\GoogleCaptcha\Classes\Rules;

use Request;
use GoogleCaptcha as GoogleCaptchaFacade;
use Illuminate\Contracts\Validation\ImplicitRule;
use Medinam\GoogleCaptcha\Classes\Exceptions\VerificationException;
use Medinam\GoogleCaptcha\Models\Settings as GoogleCaptchaSettings;
use Medinam\GoogleCaptcha\Models\WhitelistIp;

class GoogleCaptcha implements ImplicitRule
{
    private $params = [];

    /**
     * Construct a reCAPTCHA validation rule with validation parameters.
     *
     * @param array Validation parameters.
     */
    public function __construct(array $params = null)
    {
        $this->params = $params ? $params : [];
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // If Google Captcha is not active will pass all varifications
        if (!GoogleCaptchaSettings::get('is_active', false)) {
            return true;
        }

        // Check if request IP is whitelisted
        if (WhitelistIp::where('ip', Request::ip())->first()) {
            return true;
        }

        $paramAction = array_key_exists(0, $this->params) ? $this->params[0] : null;
        $paramScore = array_key_exists(1, $this->params) ? $this->params[1] : 0.5;

        if (!($paramScore >= 0.0 && $paramScore <= 1.0)) {
            throw new \InvalidArgumentException('Invalid score suplied to Google Captcha rule, '.
                    'should be between 0.0 and 1.0. Input was: ' . $paramScore);
        }

        try {
            $score = GoogleCaptchaFacade::verify($value, $paramAction);
            if ($score >= $paramScore) {
                return true;
            }
        }
        catch (VerificationException $e) {
            // This is intencionally empty
        }

        return false;
    }

    /**
     * Validation callback method.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  array  $params
     * @return bool
     */
    public function validate($attribute, $value, $params)
    {
        $this->params = $params;
        return $this->passes($attribute, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid reCAPTCHA token.';
    }
}
