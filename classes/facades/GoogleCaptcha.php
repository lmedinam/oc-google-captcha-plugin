<?php namespace Medinam\GoogleCaptcha\Classes\Facades;

use October\Rain\Support\Facade;

class GoogleCaptcha extends Facade
{
    /**
     * Get the registered name of the component.
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'medinam.googlecaptcha';
    }
}
